package main
import "fmt"

func swap(x, y  string) (string, string) {
	return y, x
}

func main() {
	var c, d string

	c, d = swap("santanu", "joydeep")
	fmt.Println(c, d)
}
