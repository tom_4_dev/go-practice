package main
import "fmt"

func main() {
	var x float64
	var a, b = 10, 20
	x = 20.5
	fmt.Println(x)
	fmt.Printf("variable is of type %T\n", x)

	fmt.Println(a)
	fmt.Printf("variable is of type %T\n", a)

	fmt.Println(b)
	fmt.Printf("variable is of type %T\n", b)
}
